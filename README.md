# shadow-cljs, proto-repl, reagent template

`shadow-cljs` is a build tool for ClojureScript.

`proto-repl` is a Clojure(Script) dev env for [Atom](https://atom.io/)

`reagent` is a ClojureScript wrapper for [React](https://reactjs.org/).

`rmwc` [is a wrapper](https://jamesmfriedman.github.io/rmwc) for the Offcial Google [Material Components for Web](https://github.com/material-components/material-components-web).

## Setup And Run
#### Copy repository
```shell
git clone https://gitlab.com/randall.mason/shadow-rmwc-reagent-example.git && cd shadow-rmwc-reagent-example
```

#### Install dependencies
```shell
yarn install || npm install
```

#### Run dev server
```shell
yarn dev || npm run dev
```

#### Compile an optimized version

```shell
yarn release || npm run release
```

